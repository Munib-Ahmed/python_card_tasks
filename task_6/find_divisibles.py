import logging
import time
from pprint import pprint


def find_divisibles(in_range, divisor):
	start = time.time()
	logging.debug('find_divisibles called with range %s and divisor %s', str(in_range), str(divisor))
	increment=divisor
	result=divisor
	resultant_list=[]
	while (result<=in_range):
		resultant_list.append(result)
		result = result+increment
	end = time.time()
	logging.debug('find_divisibles ended with range %s and divisor %s. It took %s seconds', str(in_range), str(divisor), str(end - start))
	return resultant_list

if __name__=="__main__":
	logging.basicConfig(filename='example.log',level=logging.DEBUG, filemode='w')
	find_divisibles(50800000, 34113)
	find_divisibles(100052, 3210)
	pprint(find_divisibles(500, 3))