import logging
import time
from pprint import pprint
import asyncio


async def async_find_divisibles(in_range, divisor):
	start = time.time()
	logging.debug('find_divisibles called with range %s and divisor %s', str(in_range), str(divisor))
	increment=divisor
	result=divisor
	resultant_list=[]
	while (result<=in_range):
		resultant_list.append(result)
		result = result+increment
		await asyncio.sleep(0.00001)
	end = time.time()
	logging.debug('find_divisibles ended with range %s and divisor %s. It took %s seconds', str(in_range), str(divisor), str(end - start))
	return resultant_list


async def main():
    task_1 = loop.create_task(async_find_divisibles(50800000, 34113))	#created tasks in loop
    task_2 = loop.create_task(async_find_divisibles(100052, 3210))
    task_3 = loop.create_task(async_find_divisibles(500, 3))
    await asyncio.wait([task_1,task_2,task_3])			#wait for the tasks to end
    return task_1, task_2, task_3

if __name__=="__main__":
    
	logging.basicConfig(filename='example.log',level=logging.DEBUG, filemode='w')
	loop = asyncio.get_event_loop()
	output_1, output_2, output_3=loop.run_until_complete(main())		#we get our result objects
	pprint(output_1.result())
	pprint(output_2.result())
	pprint(output_3.result())
	loop.close()