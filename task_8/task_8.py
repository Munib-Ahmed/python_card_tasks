import numpy as np
import datetime
import argparse


def statistics(args):
	print(f"{datetime.datetime.now()}")
	#with open(args.PATH) as myfile:
	#	head = [next(myfile) for x in range(100)]
	#for x in head:
	#	print(type(x))
	
	a_array = np.genfromtxt(args.PATH)[0:100]
	print("Mean: "+ str(np.mean(a_array)) + " us ")
	print("Median: "+ str(np.median(a_array)) + " us ")
	print("Min: "+ str(np.min(a_array)) + " us ")
	print("Max: "+ str(np.max(a_array)) + " us ")
	print("Standard Deviation: "+ str(np.std(a_array)) + " us ")
	print("90th percentile: "+ str(np.percentile(a_array, 90)) + " us ")
	print("99th percentile: "+ str(np.percentile(a_array, 99)) + " us ")
	print("99.9th percentile: "+ str(np.percentile(a_array, 99.9)) + " us ")
	print("99.99th percentile: "+ str(np.percentile(a_array,99.99)) + " us ")
	print("Total values: "+ str(len(a_array)))


	

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('PATH', help='Path to messages_latency.txt file')
	args = parser.parse_args()
	
	
	statistics(args)
