import asyncio
from os import path
import websockets
import json
import time
import logging
import argparse


async def message(args):
	async with websockets.connect("ws://localhost:1234") as socket:
		print("COnnected")
		f = open(args.PATH)
		data = json.load(f)
		for x in data:
			x["tx_time"] = time.time_ns()
			#msg = input("What do you want to send: ")
			await socket.send(json.dumps(x))
			print(await socket.recv())

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument('PATH', help='Path to client_messages json file')
	parser.add_argument('-c', '--console_logging', action='store_false', default=True, help="enable/disable console logging by default enabled")
	parser.add_argument('-f', '--file_logging', action='store_true', default=False, help="enable/disable console logging by default disabled")

	args = parser.parse_args()
	
	
	asyncio.get_event_loop().run_until_complete(message(args))
