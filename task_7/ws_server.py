import asyncio
from os import path
import websockets
import json
import time
import logging
import argparse


async def response(websocket, path):
	f = open('client_messages.json')
	file_msg_latency = open("msg_latency.txt", "w")
	data = json.load(f)
	#for x in data:
	async for msg in websocket:
		#message = await websocket.recv()
		msg = json.loads(msg)
		msg["rx_time"] = time.time_ns()
		msg["msg_latency"] = msg["rx_time"]-msg["tx_time"]
		#print(msg)
		#message["msg_latency"]=message["rx_time"]-message["tx_time"]
		#print(f"We got the message from the client: {message}")
		#await websocket.send("I can confirm I got your message!")
		await websocket.send(json.dumps(msg))
		file_msg_latency.write(str(msg["msg_latency"])+"\n")

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--console_logging', action='store_false', default=True, help="enable/disable console logging by default enabled")
	parser.add_argument('-f', '--file_logging', action='store_true', default=False, help="enable/disable console logging by default disabled")
	start_server = websockets.serve(response, 'localhost', 1234)
	asyncio.get_event_loop().run_until_complete(start_server)
	asyncio.get_event_loop().run_forever()
