import matplotlib.pyplot as plt

def square_function(input_list):
	operated_list=[]
	for x in range(len(input_list)):
		operated_list.append(input_list[x]**2)
	return operated_list
	

def cube_function(input_list):
	operated_list=[]
	for x in range(len(input_list)):
		operated_list.append(input_list[x]**3)
	return operated_list

x=[1,2,3,4,5,6,7,8,9,10]
squared_list=square_function(x)
print(squared_list)
cubed_list=cube_function(x)
print(cubed_list)

plt.plot(x, squared_list, label="squared line")
plt.xlabel("x")
plt.ylabel("square values")
plt.show()
    
    
plt.plot(x, cubed_list, label="cubed line")
plt.xlabel("x")
plt.ylabel("cube values")
plt.show()
