import argparse
import json
import random

def monto_carlo_simulation(interval):
    #print(interval)
    circle_points= 0
    square_points= 0

    for i in range(interval**2):
        rand_x= random.uniform(-1, 1)
        rand_y= random.uniform(-1, 1)
  
        # Distance between (x, y) from the origin
        origin_dist= rand_x**2 + rand_y**2
  
        # Checking if (x, y) lies inside the circle
        if origin_dist<= 1:
            circle_points+= 1
  
        square_points+= 1
  
        # Estimating value of pi,
        # pi= 4*(no. of points generated inside the 
        # circle)/ (no. of points generated inside the square)
        pi = 4* circle_points/ square_points
  
    ##print(rand_x, rand_y, circle_points, square_points, "-", pi)
    ##print("\n")
    print("Final Estimation of Pi=", pi) 

if __name__=="__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input_iterations", help="if flag is '-i', also enter no. of iterations")
    parser.add_argument("-j", "--Json_input", action="store_true", help="if flag is '-j', value will be taken from JSon, set as 1000 for now")
    
    args = parser.parse_args()

    #print(args.input_iterations)
    #print(args.Json_input)

    if (args.input_iterations):
        monto_carlo_simulation(int(args.input_iterations))
    elif (args.Json_input):
        with open('sample.json', 'r') as openfile:
            json_object = json.load(openfile)
            monto_carlo_simulation(int(json_object))
  



#python3 task_4.py -j
#python3 task_4.py -i 100