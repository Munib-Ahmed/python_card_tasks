import argparse
import math

parser = argparse.ArgumentParser()

parser.add_argument("UP", help="up direction movement")
parser.add_argument("DOWN", help="down direction movement")
parser.add_argument("LEFT", help="left direction movement")
parser.add_argument("RIGHT", help="right direction movement")

args = parser.parse_args()
#print(args.UP)
#print(args.DOWN)
#print(args.LEFT)
#print(args.RIGHT)

def magnitude(left, right, up, down):
    base=(left)-(right)
    perpendicular=(up)-(down)
    hypothenuse=int(math.sqrt((base**2)+(perpendicular**2)))
    return hypothenuse

print(magnitude(int(args.LEFT), int(args.RIGHT), int(args.UP), int(args.DOWN)))
