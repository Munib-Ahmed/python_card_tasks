import subprocess
import os
import getpass
import psutil

#print((subprocess.check_output("lscpu", shell=True).strip()).decode())
#print(getpass.getuser())
folder_address= "/home/"+str(getpass.getuser())+"/Details"
#print(folder_address)

if (os.path.isdir(folder_address)):
	print ("folder already exists") 
else:
	print("folder not found, creating one for you")
	os.mkdir(folder_address)

file_address = folder_address+"/summary.txt"
print(file_address)
if (os.path.isfile(file_address)):
	print("File already exists, just updating that.")
else:
	print("File doesn't exists, creating one for you.")

file_object  = open(file_address, 'w')
Information = (subprocess.check_output("lscpu", shell=True).strip()).decode()

file_object.write(str(Information))
file_object.write("\nTotal RAM installed: "+str(round(psutil.virtual_memory().total/1000000000, 2))+ "GB")

file_object.close()